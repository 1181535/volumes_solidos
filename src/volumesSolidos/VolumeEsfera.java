package volumesSolidos;

public class VolumeEsfera
{
    private Double raio;

    //construtor com parâmetro
    public VolumeEsfera(Double raio){
        this.raio = raio;
    }

    //obter raio
    public Double getRaio() {
        return raio;
    }

    public double CalcularVolumeEsfera(Double raio) {
        double volume = ( 4.0 / 3.0 ) * Math.PI * Math.pow( getRaio(), 3 );
        return volume;
    }
}

